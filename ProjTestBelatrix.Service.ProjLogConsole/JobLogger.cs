﻿using Common.Constanst;
using Common.Enums;
using ProjTestBelatrix.Domain.Contracts;
using ProjTestBelatrix.Domain.Entities;
using ProjTestBelatrix.Infraestructure.DataAcces.Repositories;
using ProjTestBelatrix.Service.ProjLogConsole.Model;
using System;
using System.IO;

namespace ProjTestBelatrix.Service.ProjLogConsole
{
	public class JobLogger {

		#region Attributes
		private static IGenericRepository<Log> _logRepository;
		private JobLoggerModel jobLoggerModel;
		#endregion

		#region Constructors
		public JobLogger(JobLoggerModel jobLoggerModel) {
			this.jobLoggerModel = jobLoggerModel;
			_logRepository = new LogRepository();
		}
		#endregion

		#region Public methods
		public ResultModel LogMessage(MessageModel message) {
			try {
				message.Message.Trim();
				var result = new ResultModel();

				result = ValidateModels(message);
				if (!result.Success) {
					return result;
				}

				return result = CreateLogInDbAndTxt(message);
			}
			catch (Exception exception) {
				return new ResultModel { Success = false, Message = exception.Message };
			}
		}
		#endregion


		#region Private methods

		private ResultModel ValidateModels(MessageModel message) {
			var result = new ResultModel { 
				Success = true
			};

			if (jobLoggerModel.LogToModel == null) {
				result.Success = false;
				result.Message = LabelErrorsConstants.INVALID_CONFIGURATION; 				
			}

			if (string.IsNullOrEmpty(message.Message)) {
				result.Success = false;
				result.Message = LabelErrorsConstants.MESSAGE_REQUIERED;
				return result;
			}

			if (jobLoggerModel.LogAdvertisementModel == null && message.OptionMessageModel == null) {
				result.Success = false;
				result.Message = LabelErrorsConstants.ERROR_OR_WARNING;
			}

			return result;
		}		

		private ResultModel CreateLogInDbAndTxt(MessageModel message) {
			string text = string.Empty;

			try {
				if (message.OptionMessageModel.HasMessage && jobLoggerModel.LogAdvertisementModel.LogMessage) {
					text = text + GetTextFile(message, Adverticement.Message);
				}

				if (message.OptionMessageModel.HasError && jobLoggerModel.LogAdvertisementModel.LogError) {
					text = text + GetTextFile(message, Adverticement.Error);
				}

				if (message.OptionMessageModel.HasWarning && jobLoggerModel.LogAdvertisementModel.LogWarning) {
					text = text + GetTextFile(message, Adverticement.Warning);
				}

				AddTextToFile(text);
				return new ResultModel { Success = true, Message = LabelSuccesConstants.OPERATION_SUCCESS };
			}
			catch (Exception exception) {
				return new ResultModel {
					Success = false,
					Message = exception.Message
				};
			}
		}


		private string GetTextFile(MessageModel message, Adverticement adverticement) {
			AddLogInDataBase(message, adverticement);
			return DateTime.Now.ToShortDateString() + message;
		}

		private static bool AddLogInDataBase(MessageModel message, Adverticement adverticement) {
			bool validate = false;

			validate = _logRepository.Add(new Log {
				Message = message.Message,
				Type = adverticement.ToString()
			});

			return validate;
		}

		private static void AddTextToFile(string text) {
			string path = ValidateOrCreateFile();
			string allText = path + text;

			File.WriteAllText(path, allText);
		}

		private static string ValidateOrCreateFile() {
			string path = System.Configuration.ConfigurationManager.AppSettings["LogFileDirectory"];
			string fileName = $"LogFile{ DateTime.Now.ToShortDateString().Replace("/", "") }.txt";
			string fileFullPath = path + fileName;

			if (File.Exists(fileFullPath)) {
				return fileFullPath;
			} else {
				var newFile = File.Create(fileFullPath);
				newFile.Dispose();

				return newFile.Name;
			}
		}
		#endregion

	}
}
