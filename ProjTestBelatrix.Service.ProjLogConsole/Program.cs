﻿using System;

namespace ProjTestBelatrix.Service.ProjLogConsole
{
	class Program
	{
		static void Main(string[] args) {
			CreateLog();

			Console.ReadLine();
		}

		#region Private Methods
		private static void CreateLog() {
			var jobLogger = new JobLogger(new Model.JobLoggerModel {
				LogToModel = new Model.LogToModel {
					LogToConsole = true,
					LogToDatabase = true,
					LogToFile = true
				},
				LogAdvertisementModel = new Model.LogAdvertisementModel {
					LogError = true,
					LogMessage = true,
					LogWarning = true
				}
			});

			var result = jobLogger.LogMessage(new Model.MessageModel {
				Message = "Ocurrio un error en alguna parte...",
				OptionMessageModel = new Model.OptionMessageModel
				{
					HasError = true,
					HasMessage = true,
					HasWarning = true
				}
			});

			Console.WriteLine(result.Message);
		}
		#endregion

	}
}
