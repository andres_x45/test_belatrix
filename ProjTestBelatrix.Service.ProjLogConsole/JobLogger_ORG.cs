﻿using ProjTestBelatrix.Service.ProjLogConsole.Model;
using System;

namespace ProjTestBelatrix.Service.ProjLogConsole
{
	public class JobLogger_ORG
	{
		// los atributos se pueden en un modelo
		private static bool _logToFile;
		private static bool _logToConsole;
		private static bool _logMessage;
		private static bool _logWarning;
		private static bool _logError;
		private static bool LogToDatabase;

		// new version JobLoggerModel
		private static JobLoggerModel _jobLoggerModel;

		private bool _initialized;

		// se debe refactorizar el codigo teniendo en cuenta lo siguiente

		public JobLogger_ORG(bool logToFile, bool logToConsole, bool logToDatabase, bool logMessage, bool logWarning, bool logError)
		{
			_logError = logError;
			_logMessage = logMessage;
			_logWarning = logWarning;
			LogToDatabase = logToDatabase;
			_logToFile = logToFile;
			_logToConsole = logToConsole;
		}

		public JobLogger_ORG(JobLoggerModel jobLoggerModel) {
			_jobLoggerModel = jobLoggerModel;
		}

		// corregir el nombre de la variable message pues es de tipo string y tambien es boolean 
		// Realizar un separación de capas en donde se pueda tener control del acceso a datos.
		// Cambiar la conexion a datos haciendo uso del ORM Entity Framwwork
		// Agregar patron de diseño repositorio para el accesos a la base de datos.
		// mejorar logica del metodo LogMessage es un metodo muy extenso, no cumple con la practcica de principios solid.
		public static void LogMessage(string message, bool hasMessage, bool hasWarning, bool hasError) {
			message.Trim();
			if (message == null || message.Length == 0)
			{
				return; 
			}

			if (!_logToConsole && !_logToFile && !LogToDatabase) {
				throw new Exception("Invalid configuration");
			}

			if ((!_logError && !_logMessage && !_logWarning) || (!hasMessage && !hasWarning && !hasError))
			{
				throw new Exception("Error or Warning or Message must be specified");
			}

			System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
			connection.Open();
			int t = 0;
			if (hasMessage && _logMessage)
			{
				t = 1;
			}
			if (hasError && _logError)
			{
				t = 2;
			}
			if (hasWarning && _logWarning)
			{
				t = 3;
			}
			System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("Insert into Log Values('" + message + "', " + t.ToString() + ")");
			command.ExecuteNonQuery();

			string l = string.Empty;
			if (!System.IO.File.Exists(System.Configuration.ConfigurationManager.AppSettings["LogFileDirectory"] + "LogFile" + DateTime.Now.ToShortDateString() + ".txt"))
			{
				l = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["LogFileDirectory"] + "LogFile" + DateTime.Now.ToShortDateString() + ".txt");
			}

			if (hasError && _logError)
			{
				l = l + DateTime.Now.ToShortDateString() + message;
			}
			if (hasWarning && _logWarning)
			{
				l = l + DateTime.Now.ToShortDateString() + message;
			}
			if (hasMessage && _logMessage)
			{
				l = l + DateTime.Now.ToShortDateString() + message;
			}

			System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["LogFileDirectory"] + "LogFile" + DateTime.Now.ToShortDateString() + ".txt", l);

			if (hasError && _logError)
			{
				Console.ForegroundColor = ConsoleColor.Red;
			}
			if (hasWarning && _logWarning)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
			}
			if (hasMessage && _logMessage)
			{
				Console.ForegroundColor = ConsoleColor.White;
			}
			Console.WriteLine(DateTime.Now.ToShortDateString() + message);
		}
	}
}
