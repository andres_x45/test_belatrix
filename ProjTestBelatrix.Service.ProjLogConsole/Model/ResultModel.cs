﻿using System;

namespace ProjTestBelatrix.Service.ProjLogConsole.Model
{
	public class ResultModel {
		public bool Success { get; set; }
		public string Message { get; set; }
	}
}
