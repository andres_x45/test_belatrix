﻿namespace ProjTestBelatrix.Service.ProjLogConsole.Model
{
	public class JobLoggerModel
	{
		public LogToModel LogToModel { get; set; }
		public LogAdvertisementModel LogAdvertisementModel { get; set; }
	}
}
