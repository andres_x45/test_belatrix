﻿namespace ProjTestBelatrix.Service.ProjLogConsole.Model
{
	public class LogToModel {
		public bool LogToFile { get; set; }
		public bool LogToConsole { get; set; }
		public bool LogToDatabase { get; set; }
	}
}
