﻿using System.ComponentModel.DataAnnotations;

namespace ProjTestBelatrix.Service.ProjLogConsole.Model
{
	public class MessageModel {
		public string Message { get; set; }
		public OptionMessageModel OptionMessageModel{ get; set; }
	}

	public class OptionMessageModel {
		public bool HasMessage { get; set; }
		public bool HasWarning { get; set; }
		public bool HasError { get; set; }
	}
}
