﻿namespace ProjTestBelatrix.Service.ProjLogConsole.Model
{
	public class LogAdvertisementModel {
		public bool LogMessage { get; set; }
		public bool LogWarning { get; set; }
		public bool LogError { get; set; }
	}
}
