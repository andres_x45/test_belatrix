namespace ProjTestBelatrix.Infraestructure.DataAcces
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    // [Table("Log")]
    public partial class Log_1
    {
        public int Id { get; set; }

        public string Message { get; set; }

        [StringLength(20)]
        public string Type { get; set; }

        public DateTime? Date { get; set; }
    }
}
