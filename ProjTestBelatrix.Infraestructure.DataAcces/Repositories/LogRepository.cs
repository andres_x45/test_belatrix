﻿using ProjTestBelatrix.Domain.Contracts;
using ProjTestBelatrix.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjTestBelatrix.Infraestructure.DataAcces.Repositories
{
	public class LogRepository : IGenericRepository<Log>
	{
		private readonly ModelLogger _context;

		public LogRepository() {
			_context = new ModelLogger();
		}

		public bool Add(Log entity) {
			int result = 0;
			entity.Date = DateTime.UtcNow;

			_context.Log.Add(entity);
			result = _context.SaveChanges();

			return (result > 0);
		}

		public bool Update(Log entity)
		{
			throw new NotImplementedException();
		}

		public List<Log> GetAll()
		{
			throw new NotImplementedException();
		}


	}
}
