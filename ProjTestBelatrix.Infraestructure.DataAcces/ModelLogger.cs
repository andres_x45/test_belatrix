namespace ProjTestBelatrix.Infraestructure.DataAcces
{
	using ProjTestBelatrix.Domain.Entities;
	using System.Data.Entity;

	public partial class ModelLogger : DbContext
	{
		public ModelLogger()
			: base("name=ModelLogger")
		{
		}

		public virtual DbSet<Log> Log { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Log>()
				.Property(e => e.Message)
				.IsUnicode(false);

			modelBuilder.Entity<Log>()
				.Property(e => e.Type)
				.IsUnicode(false);
		}
	}
}
