﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjTestBelatrix.Domain.Contracts
{
	public interface IGenericRepository<T> where T : class
	{
		bool Add(T entity);
		bool Update(T entity);
		List<T> GetAll();
	}
}
