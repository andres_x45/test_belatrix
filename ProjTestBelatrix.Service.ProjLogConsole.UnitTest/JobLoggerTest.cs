﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjTestBelatrix.Service.ProjLogConsole.Model;

namespace ProjTestBelatrix.Service.ProjLogConsole.UnitTest
{
	[TestClass]
	public class JobLoggerTest
	{
		public JobLoggerModel jobLoggerModel = new JobLoggerModel { 
			LogToModel = null,
			LogAdvertisementModel = null
		};

		[TestMethod]
		public void ValidateModels_WhenLogToModelIsNull_ReturnFalseInPropertySuccessInResultModel() {
			var jobLogger = new JobLogger(jobLoggerModel);
			var privateObject = new PrivateObject(jobLogger);

			var messageModel = new MessageModel {
				Message = "Esto es una prueba unitaria",
				OptionMessageModel = new OptionMessageModel {
					HasError = true,
					HasMessage = true,
					HasWarning = true
				}
			};
			var result = privateObject.Invoke("ValidateModels", messageModel);

			Assert.AreNotEqual(messageModel, result);
		}
	}
}
